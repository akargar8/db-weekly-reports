import cx_Oracle
import sys
import xlsxwriter as ex
import datetime
import os


USERNAME = "c##servicedesk"
PASSWORD = ""

PRDENG_HOST= ""
PRDEND_SRV= "prdeng02"

PRDAPP_HOST= ""
PRDAPP_SRV= "prdapp00"

PRDASY_HOST= ""
PRDASY_SRV= "prdasy00"


try:
    print("[+] Connecting to PRDASY ...")
    prdasy_con = cx_Oracle.connect(USERNAME, PASSWORD, PRDASY_HOST+"/"+PRDASY_SRV)
    prdasy_cur = prdasy_con.cursor()
    print("[+] Connected to PRDASY\n")

    print("[+] Connecting PRDENG ...")
    prdeng_con = cx_Oracle.connect(USERNAME, PASSWORD, PRDENG_HOST+"/"+PRDEND_SRV)
    prdeng_cur = prdeng_con.cursor()
    print("[+] Connected to PRDENG\n")

    print("[+] Connecting to PRDAPP ...")
    prdapp_con = cx_Oracle.connect(USERNAME, PASSWORD, PRDAPP_HOST+"/"+PRDAPP_SRV)
    prdapp_cur = prdapp_con.cursor()
    print("[+] Connected to PRDAPP\n")

    prd_list = [prdasy_cur, prdapp_cur, prdeng_cur, ]
except Exception as e:
    print(e)
    sys.exit(0)


def create_dir():
    dir_name = "Database_Report_{}".format(datetime.date.today())

    if os.path.exists(dir_name):
        pass
    else:
        os.mkdir(dir_name)
    os.chdir(dir_name)


def report_1 ():
    print("[+] Starting Report 1 ...")
    HEADER_1 = ["sql_text", "sql_id", "execution_cost_ratio", "schema_name", "pdb_name", "execution_plan",
                "alternative",
                "benefit", "new_execution_plan"]
    filename_report_1 = "Midrp_weekly_sql_performance_report_{}".format(datetime.date.today())
    row = 0
    workbook = ex.Workbook('{}.xlsx'.format(filename_report_1))
    worksheet = workbook.add_worksheet('worst_sql_with_ratio')
    worksheet.set_column(0,0,95)
    worksheet.set_column(1,1,30)
    worksheet.set_column(2, 2,30)
    worksheet.set_column(3, 3, 30)
    worksheet.set_column(4, 4, 30)
    worksheet.set_column(5, 5, 90)
    worksheet.set_column(6, 6, 30)
    worksheet.set_column(7, 7, 30)
    worksheet.set_column(8, 8, 30)
    worksheet.set_default_row(40)


    header_format = workbook.add_format({
        'bold' : True,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#BFBFBF',
    })
    cell_format = workbook.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
    })
    merge_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'bg_color': 'black',
        'font_color': 'white'
    })

    for prd in prd_list:

        prd_name = str(prd).split(" ")[-1].split("/")[1].split(">>")[0]
        print("[*] Creating Report from {} ...".format(prd_name))
        cmp = [] #List to Compare SQL Texts
        sql_id = []
        row += 1
        column =0
        #worksheet.write(row, column, prd_name.upper(), merge_format) # Write Database name
        worksheet.merge_range('A{}:I{}'.format(row,row), prd_name.split("00")[0].upper(), merge_format)
        for item in HEADER_1:
            worksheet.write(row, column, item, header_format) #Write Header
            column += 1
        for index , result in enumerate(prd.execute('select * from vw_sql_performance_report')):
            column = 0
            #row += 1
            if result[0] not in cmp:
                row +=1
                for item in result:
                    worksheet.write(row, column, item, cell_format)
                    column +=1
                cmp.append(result[0])
                sql_id.append(result[1])
            if row % 12 == 0:
                break
        exec_plan(prd, workbook, worksheet, sql_id, row)
        print("[-] Report Successfuly Created\n")
    workbook.close()
    print("[-] DONE\n")
def exec_plan(prd, wb, ws, sql_id, row):
    cell_format = wb.add_format({
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap' : True
    })
    for sid in sql_id :
        r = []
        for item in prd.execute("select * from table(DBMS_XPLAN.DISPLAY_CURSOR('{}'))".format(sid)):
            r.append("\n".join(item))
        ws.write(row-10, 5, "\n".join(r), cell_format)
        row +=1
    return

def report_2 ():

    print("[+]Starting Report 2 ... \n")
    # sheets_and_headers = {"1_high_load_sql_ids" : ["PDB_NAME", "PARSING_SCHEMA_NAME", "SQL_ID", "CPU_RANK",
    #                                              "MAX_CPU_TIME_MILLISEC", "ELAPSED_RANK", "MAX_ELAPSED_TIME_MILLISEC"],
    #                       "2_high_load_sql_texts" : ["SQL_ID", "SQL_TEXT"],
    #                       "3_number_of_sessions" : ["PDB_NAME", "USERNAME", "SESS_NO"],
    #                       "4_number_of_open_cursors" : ["PDBNAME", "USERNAME", "MACHINE", "TOTAL_CUR", "AVG_CUR", "MAX_CUR"],
    #                       "5_table_size" : ["PDB_NAME", "OWNER", "OBJECT_NAME", "OBJECT_TYPE", "COLUMN_NAME", "OBJECT_SIZE"]}
    header_1 = ["PDB_NAME", "PARSING_SCHEMA_NAME", "SQL_ID", "CPU_RANK",
                "MAX_CPU_TIME_MILLISEC", "ELAPSED_RANK", "MAX_ELAPSED_TIME_MILLISEC"]
    header_2 = ["SQL_ID", "SQL_TEXT"]
    header_3 = ["PDB_NAME", "USERNAME", "SESS_NO"]
    header_4 = ["PDBNAME", "USERNAME", "MACHINE", "TOTAL_CUR", "AVG_CUR", "MAX_CUR"]
    header_5 = ["PDB_NAME", "OWNER", "OBJECT_NAME", "OBJECT_TYPE", "COLUMN_NAME", "OBJECT_SIZE"]

    for prd in prd_list:
        prd_name = str(prd).split(" ")[-1].split("/")[1].split(">>")[0]
        print("[+]Creating Report From {}".format(prd_name))
        filename_report_2 = "Midrp_{0}_weekly_performance_report_{1}".format(prd_name, datetime.date.today())
        workbook = ex.Workbook('{}.xlsx'.format(filename_report_2))

        header_format = workbook.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': '#CFE0F1',
            'font' : 'Tahoma',
            'font_size' : '10'
        })
        cell_format = workbook.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font' : 'Tahoma',
            'font_size' : 10
        })
        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font' : 'Calibri',
            'font_size' : 20
        })

        high_load_sql_id(prd, workbook, header_1, merge_format, header_format, cell_format)
        high_load_sql_txt(prd, workbook, header_2, merge_format, header_format, cell_format)
        number_of_sessions(prd, workbook, header_3, merge_format, header_format, cell_format)
        number_of_open_cursors(prd, workbook, header_4, merge_format, header_format, cell_format)
        table_size(prd, workbook, header_5, merge_format, header_format, cell_format)

        workbook.close()
        print("[-] DONE")
def high_load_sql_id (prd, wb, header, merge_format, header_format, cell_format) :
    row = 1
    column = 0
    worksheet = wb.add_worksheet("high_load_sql_id")
    worksheet.merge_range('A{}:G{}'.format(row, row), "High Load SQL ID (Last 7 Days) ", merge_format)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 30)
    worksheet.set_column(3, 3, 10)
    worksheet.set_column(4, 4, 30)
    worksheet.set_column(5, 5, 10)
    worksheet.set_column(6, 6, 30)

    for h in header :
        worksheet.write(row, column, h, header_format)
        column +=1
    row +=1

    for result in prd.execute("select * from vw_weekly_performance_report_sheet1"):
        column = 0
        for item in result :
            if isinstance(item, cx_Oracle.LOB):
                a = (item.read())
                worksheet.write(row, column, str(a), cell_format)
            else:
                worksheet.write(row, column, item, cell_format)
            column +=1
        row += 1

        worksheet.write_formula(row, 3, '=SUM(D3:D{})'.format(row), header_format)
        worksheet.write_formula(row, 4, '=SUM(E3:E{})'.format(row), header_format)
        worksheet.write_formula(row, 5, '=SUM(F3:F{})'.format(row), header_format)
        worksheet.write_formula(row, 6, '=SUM(G3:G{})'.format(row), header_format)

def high_load_sql_txt (prd, wb, header, merge_format, header_format, cell_format) :
    row = 1
    column = 0
    worksheet = wb.add_worksheet(" high_load_sql_texts")
    worksheet.merge_range('A{}:B{}'.format(row, row), "High Load SQL Texts (Last 7 Days)", merge_format)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 220)

    for h in header:
        worksheet.write(row, column, h, header_format)
        column += 1
    row += 1

    for result in prd.execute("select * from vw_weekly_performance_report_sheet2"):
        column = 0
        for item in result :
            if isinstance(item, cx_Oracle.LOB):
                a = (item.read())
                worksheet.write(row, column, str(a), cell_format)
            else:
                worksheet.write(row, column, item, cell_format)
            column +=1
        row += 1


def number_of_sessions  (prd, wb, header, merge_format, header_format, cell_format):
    row = 1
    column = 0
    worksheet = wb.add_worksheet("number_of_sessions")
    worksheet.merge_range('A{}:C{}'.format(row, row), "Number of Sessions (Last 7 Days)", merge_format)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 30)

    for h in header:
        worksheet.write(row, column, h, header_format)
        column += 1
    row += 1

    for result in prd.execute("select * from vw_weekly_performance_report_sheet3"):
        column = 0
        for item in result :
            if isinstance(item, cx_Oracle.LOB):
                a = (item.read())
                worksheet.write(row, column, str(a), cell_format)
            else:
                worksheet.write(row, column, item, cell_format)
            column +=1
        row += 1

    worksheet.write_formula(row, 2, '=SUM(C3:C{})'.format(row), header_format)

def number_of_open_cursors (prd, wb, header, merge_format, header_format, cell_format) :
    row = 1
    column = 0
    worksheet = wb.add_worksheet("number_of_open_cursors")
    worksheet.merge_range('A{}:F{}'.format(row, row), "Number Of Open Cursors (Last 7 Days)", merge_format)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 30)
    worksheet.set_column(3, 3, 10)
    worksheet.set_column(4, 4, 10)
    worksheet.set_column(5, 5, 10)

    for h in header:
        worksheet.write(row, column, h, header_format)
        column += 1
    row += 1

    for result in prd.execute("select * from vw_weekly_performance_report_sheet4"):
        column = 0
        for item in result:
            if isinstance(item, cx_Oracle.LOB):
                a = (item.read())
                worksheet.write(row, column, str(a), cell_format)
            else:
                worksheet.write(row, column, item, cell_format)
            column += 1
        row += 1

    worksheet.write_formula(row, 3, '=SUM(D3:D{})'.format(row), header_format)
    worksheet.write_formula(row, 4, '=SUM(E3:E{})'.format(row), header_format)
    worksheet.write_formula(row, 5, '=SUM(F3:F{})'.format(row),header_format)

def table_size  (prd, wb, header, merge_format, header_format, cell_format):
    row = 1
    column = 0
    worksheet = wb.add_worksheet("table_size")
    worksheet.merge_range('A{}:F{}'.format(row, row), "Table Size per Schema (Current)", merge_format)

    worksheet.set_column(0, 0, 30)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 50)
    worksheet.set_column(3, 3, 20)
    worksheet.set_column(4, 4, 40)
    worksheet.set_column(5, 5, 20)

    for h in header:
        worksheet.write(row, column, h, header_format)
        column += 1
    row += 1

    for result in prd.execute("select * from vw_weekly_performance_report_sheet5"):
        column = 0
        for item in result:
            if isinstance(item, cx_Oracle.LOB):
                a = (item.read())
                worksheet.write(row, column, str(a), cell_format)
            else:
                worksheet.write(row, column, item, cell_format)
            column += 1
        row += 1

    worksheet.write_formula(row, 5, '=SUM(F3:F{})'.format(row), header_format)

def mail(filename):
    pass


def main() :
    create_dir()
    report_1()
    report_2()

if __name__ == "__main__" :
    main()
